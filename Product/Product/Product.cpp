#include "Product.h"

Product::Product(uint16_t id, const std::string& name, float price, uint16_t vat, std::string& dateOrType) :
	m_id(id), m_name(name), m_price(price), m_vat(vat), m_dateOrType(dateOrType)
{
}