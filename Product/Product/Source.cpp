#include "Product.h"
#include <iostream>
#include <deque>
#include <conio.h>
#include <fstream>

int main()
{
	std::deque <Product> products;

	uint16_t id;
	std::string name;
	float price;
	uint16_t vat;
	std::string dateOrType;
	for (std::ifstream inputFile("product.prodb"); !inputFile.eof();)
	{
		inputFile >> id >> name >> price >> vat >> dateOrType;
		products.emplace_back(id, name, price, vat, dateOrType);
	}
}